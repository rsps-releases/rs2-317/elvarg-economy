# Elvarg - Economy

---

**This was released on [Rune-Server](https://www.rune-server.ee/runescape-development/rs2-server/downloads/660901-economy-version-elvarg-framework-some-skills-improved-core.html) by [Professor Oak](https://www.rune-server.ee/members/professor+oak/).**

---

>  The goal with this sideproject was just to try to build a nice economy OSRS server using my Elvarg framework. Seeing how I won't have time to work on this anymore, I really hope someone does.
> 
> Make sure to read the list below for a quick update log. These are only the things I have on the top of my head right now. A lot more has been done.
TLDR; Better core and 11 skills added. Also converted to economy.